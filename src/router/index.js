import { createRouter, createWebHistory } from 'vue-router'
import UserSearch from '../view/search/UserSearch'
import UserSubmit from '../view/submit/UserSubmit'
import UserResult from '../view/result/UserResult'
import UserIndex from '../view/index/UserIndex'
import UserProfile from '../view/profile/UserProfile'
import UserAccountLoginView from '../view/user/account/UserAccountLoginView'
import UserAccountRegisterView from '../view/user/account/UserAccountRegisterView'
import store from '../store/index'
import TempView from '../view/user/temp/TempView'
import AdminExamine from '../view/user/admin/AdminExamine'
import AdminSearch from '../view/user/admin/AdminSearch'


const routes = [

  {
    path: "/",
    name:"home_login",
    redirect: "/user/account/login/",
  },

  {
    path: "/user/admin/examine/",
    name:"user_admin_examine",
    component: AdminExamine,
    meta: {
      requestAuth:true,
    }
  },

  {
    path: "/user/admin/search/",
    name:"user_admin_search",
    component: AdminSearch,
    meta: {
      requestAuth:true,
    }
  },


  {
    path: "/user/temp/",
    name:"temp",
    component: TempView,
    meta: {
      requestAuth:true,
    }
  },

  {
    path: "/search/",
    name: "search",
    component: UserSearch,
    meta: {
      requestAuth:true,
    }
  },

  {
    path: "/submit/",
    name: "submit",
    component: UserSubmit,
    meta: {
      requestAuth:true,
    }
  },

  {
    path: "/result/",
    name: "result",
    component: UserResult,
    meta: {
      requestAuth:true,
    }
  },

  {
    path: "/index/",
    name:"index",
    component: UserIndex
  },

  {
    path: "/profile/",
    name:"profile",
    component: UserProfile,
    meta: {
      requestAuth:true,
    }
  },

  {
    path: "/user/account/register/",
    name: "user_account_register",
    component: UserAccountRegisterView,
  },

  {
    path: "/user/account/login/",
    name: "user_account_login",
    component: UserAccountLoginView,
  },
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

router.beforeEach((to, from, next) =>{
  if(to.meta.requestAuth && !store.state.user.is_login){
    next({name: "user_account_login"});
  }
  else{
    next();
  }
})

export default router
